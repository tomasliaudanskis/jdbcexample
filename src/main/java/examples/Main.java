package examples;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import examples.model.Project;
import examples.repositories.ProjectRepository;
import static examples.utils.DatabaseUtils.JDBC_URI;
import static examples.utils.DatabaseUtils.PASSWORD;
import static examples.utils.DatabaseUtils.USERNAME;

public class Main {
	private static String GET_PROJECTS = "select * from projects";

	public static void main(String[] args){
		ProjectRepository projectRepository = new ProjectRepository();
		System.out.println("Project nr 2");
		System.out.println(projectRepository.findById(2));

		System.out.println("All projects");
		for (Project project : projectRepository.findAll()){
			System.out.println(project);
		}

		String newProjectDescription = "Naujas bet blogas";
		System.out.println("Insert new " + newProjectDescription);
		projectRepository.add(newProjectDescription);

		System.out.println("All projects after update");
		for (Project project : projectRepository.findAll()){
			System.out.println(project);
		}


	}




	public static void main2(String[] args){
		try {
			Connection connection = DriverManager.getConnection(JDBC_URI,
			                                                    USERNAME, PASSWORD);

			Statement stmt = connection.createStatement();
			//ResultSet rs = stmt.executeQuery("select employeeId, lastName from employees where firstName = 'John'");
			ResultSet rs = stmt.executeQuery(GET_PROJECTS);

			while (rs.next()){
				Integer projectId = rs.getInt("projectId");
				String description = rs.getString("description");

				System.out.println("Project found: " + projectId + " - " + description);
			}
			rs.close();
			stmt.close();
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
}
