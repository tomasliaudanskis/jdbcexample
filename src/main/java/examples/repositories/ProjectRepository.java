package examples.repositories;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import examples.model.Project;
import static examples.utils.DatabaseUtils.JDBC_URI;
import static examples.utils.DatabaseUtils.PASSWORD;
import static examples.utils.DatabaseUtils.USERNAME;

public class ProjectRepository {
	public void add(String description) {
		try {
			Connection connection = DriverManager.getConnection(JDBC_URI,
			                                                    USERNAME, PASSWORD);
			Statement stmt = connection.createStatement();
			int result = stmt.executeUpdate("insert into projects(description) values ('" + description + "');");

			stmt.close();
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public Project findById(int id) {
		Project result = new Project();
		try {
			Connection connection = DriverManager.getConnection(JDBC_URI,
			                                                    USERNAME, PASSWORD);

			Statement stmt = connection.createStatement();
			//ResultSet rs = stmt.executeQuery("select employeeId, lastName from employees where firstName = 'John'");
			ResultSet rs = stmt.executeQuery("select * from projects where projectId = " + id);

			while (rs.next()){
				result.projectId = rs.getInt("projectId");
				result.description = rs.getString("description");
			}
			rs.close();
			stmt.close();
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return result;

	}
	public List<Project> findAll(){
		List<Project> result = new ArrayList<Project>();
		try {
			Connection connection = DriverManager.getConnection(JDBC_URI,
			                                                    USERNAME, PASSWORD);

			Statement stmt = connection.createStatement();
			//ResultSet rs = stmt.executeQuery("select employeeId, lastName from employees where firstName = 'John'");
			ResultSet rs = stmt.executeQuery("select * from projects");

			while (rs.next()){
				Project project = new Project();
				project.projectId = rs.getInt("projectId");
				project.description = rs.getString("description");
				result.add(project);
			}
			rs.close();
			stmt.close();
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return result;

	}
}
