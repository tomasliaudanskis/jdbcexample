package examples.model;

public class Project {
	public int projectId;
	public String description;

	@Override
	public String toString() {
		return "Project id = " + projectId + "; Description = " + this.description;
	}
}
